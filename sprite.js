

function cambiarMario(){

    //Crear un objeto que haga referencia al elemento HTML cuyo ID es spriteMario
    let mario=document.getElementById('spriteMario');

    //Crear un objeto con todos los estilos que tiene el objeto
    let estilosMario=window.getComputedStyle(mario)

    //crear una variable para guardar la cadena de texto de la propiedad background-position

    let estiloFondo = estilosMario.getPropertyValue("background-position");

    console.log(estiloFondo);

    //separar las dos cadenas de texto que corresponde a las posiciones X,Y
    let coordenadas=estiloFondo.split(" ");

    let x= coordenadas[0];
    let y= coordenadas[1];

    //remover las letras px
    x=x.substring(0, x.length-2);
    y=y.substring(0, y.length-2);

    //Convertir a número

    x= Number.parseInt(x);
    y= Number.parseInt(y);

    //Calcular las nuevas coordenadas del fondo
    
    x=x-50;

    //Mofiicar el estilo "background-position" del objeto
    mario.style.backgroundPosition=x+ "px "+y+ "px";


}